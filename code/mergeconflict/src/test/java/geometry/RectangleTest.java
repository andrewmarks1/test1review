package geometry;
import static org.junit.Assert.*;
import org.junit.Test;
public class RectangleTest {
    @Test
    public void testSides(){
        Rectangle r = new Rectangle(3, 6);
        assertEquals(3, r.getSide1());
        assertEquals(6, r.getSide2());
    }

    @Test
    public void testArea(){
        Rectangle r = new Rectangle(3, 6);
        assertEquals(18, r.getArea());
    }

    @Test
    public void testToString(){
        Rectangle r = new Rectangle(3, 6);
        assertEquals("This rectangle has a height of 3 and a width of 6", r.toString());
    }
    
}
