package geometry;

import org.junit.Test;
import static org.junit.Assert.*;

public class SquareTest {
    
    @Test
    public void testingGetMethod(){
        Square square = new Square(4.0);
        assertEquals(4.0, square.getSide(), 0.00001);
    }

    @Test
    public void testingGetArea(){
        Square square = new Square(4.0);
        assertEquals(16.0, square.getArea(), 0.00001);
    }

    @Test
    public void testingToString(){
        Square square = new Square(4.0);
        assertEquals("This square has sides that measure: 4.0", square.toString());
    }

}
