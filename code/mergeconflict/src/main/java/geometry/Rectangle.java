package geometry;

public class Rectangle {
    private int side1;
    private int side2;

    public Rectangle(int side1, int side2){
        this.side1 = side1;
        this.side2 = side2;
    }

    public int getArea(){
        return this.side1 * this.side2;
    }

    public int getSide1(){
        return this.side1;
    }

    public int getSide2(){
        return this.side2;
    }

    public String toString(){
        return "This rectangle has a height of " + side1 + " and a width of " + side2;
    }
}
