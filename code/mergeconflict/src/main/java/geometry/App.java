package geometry;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        Square square = new Square(4);
        System.out.println(square);
        Rectangle r = new Rectangle(4, 2);
        System.out.println(r);
    }
}
